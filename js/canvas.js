(function() {
    var l = document.querySelector("link[rel*='icon']") || document.createElement('link');
    l.type = 'image/x-icon';
    l.rel = 'shortcut icon';
    l.href = 'https://js.cools1te.pages.dev/canvasfavicon.ico';
    document.getElementsByTagName('head')[0].appendChild(l);
    document.title = 'Dashboard';
})();